package TimeOn.TimingService.rest;

import TimeOn.TimingService.dto.TimePicking;
import TimeOn.TimingService.services.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TimeController {

    private final TimeService timeService;

    @Autowired
    public TimeController(TimeService timeService) {
        this.timeService = timeService;
    }

    @RequestMapping(value = "/meetingForOutLook", method = RequestMethod.POST)
    public ResponseEntity meetingForOutLook(@RequestBody TimePicking timePicking) {
        return ResponseEntity.ok(timeService.meetingForOutLook(timePicking));
    }

    @RequestMapping(value = "/meetingForTimeOn", method = RequestMethod.POST)
    public ResponseEntity meetingForTimeOn(@RequestBody TimePicking timePicking) {
        return ResponseEntity.ok(timeService.meetingForTimeOn(timePicking));
    }
}
