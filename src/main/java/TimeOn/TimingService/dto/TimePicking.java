package TimeOn.TimingService.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Data
@EqualsAndHashCode
@ToString
public class TimePicking {
    private List<UserTime> events;
    private Integer priority;
    private LocalDateTime begin;
    private LocalDateTime end;
    private Integer duration;
}
