package TimeOn.TimingService.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Data
@EqualsAndHashCode
@ToString
public class UserTime {
    private Integer id;
    private List<Events> events;
}
