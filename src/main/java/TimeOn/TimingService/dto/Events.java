package TimeOn.TimingService.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode
@ToString
public class Events {
    private LocalDateTime begin;
    private LocalDateTime end;
}
