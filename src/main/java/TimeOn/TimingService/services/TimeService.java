package TimeOn.TimingService.services;

import TimeOn.TimingService.dto.Events;
import TimeOn.TimingService.dto.TimePicking;
import TimeOn.TimingService.dto.UserTime;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Service
public class TimeService {

    public TreeMap meetingForOutLook(TimePicking timePicking) {
        TreeMap<LocalDateTime, Integer> mas = new TreeMap<>();
        LocalDateTime begin = timePicking.getBegin().withSecond(0).withNano(0);
        LocalDateTime end = timePicking.getEnd().withSecond(0).withNano(0);
        LocalDateTime dateTime = begin;
        while (dateTime.isBefore(end)) {
            mas.put(dateTime, 0);
            dateTime = dateTime.plusMinutes(5);
        }
        List<UserTime> peopleUsertime = timePicking.getEvents();
        for (UserTime userTime : peopleUsertime) {
            for (Events eventInfoDTO1 : userTime.getEvents()) {
                if (eventInfoDTO1.getBegin().withSecond(0).withNano(0).isBefore(begin) && (eventInfoDTO1.getEnd().withSecond(0).withNano(0).isAfter(begin))) {
                    if (eventInfoDTO1.getEnd().withSecond(0).withNano(0).isAfter(end)) {
                        for (Map.Entry entry : mas.entrySet()) {
                            mas.replace((LocalDateTime) entry.getKey(), (Integer) entry.getValue() + 10);
                        }
                    } else {
                        LocalDateTime temp = begin.withSecond(0).withNano(0);
                        while (temp.isBefore(eventInfoDTO1.getEnd().withSecond(0).withNano(0))) {
                            mas.replace(temp, mas.get(temp) + 10);
                            temp = temp.plusMinutes(5);
                        }
                    }
                } else if (eventInfoDTO1.getBegin().withSecond(0).withNano(0).isBefore(end) && eventInfoDTO1.getEnd().withSecond(0).withNano(0).isAfter(end)) {
                    if (eventInfoDTO1.getBegin().withSecond(0).withNano(0).isBefore(begin)) {
                        for (Map.Entry entry : mas.entrySet()) {
                            mas.replace((LocalDateTime) entry.getKey(), (Integer) entry.getValue() + 10);
                        }
                    } else {
                        LocalDateTime temp = begin.withSecond(0).withNano(0);
                        while (temp.isBefore(eventInfoDTO1.getBegin().withSecond(0).withNano(0)))
                            temp = temp.plusMinutes(5);
                        while (temp.isBefore(end)) {
                            mas.replace(temp, mas.get(temp) + 10);
                            temp = temp.plusMinutes(5);
                        }
                    }
                } else {
                    LocalDateTime temp = begin.withSecond(0).withNano(0);
                    while (temp.isBefore(eventInfoDTO1.getBegin().withSecond(0).withNano(0)))
                        temp = temp.plusMinutes(5);
                    while (temp.isBefore(eventInfoDTO1.getEnd().withSecond(0).withNano(0))) {
                        mas.replace(temp, mas.get(temp) + 10);
                        temp = temp.plusMinutes(5);
                    }
                }
            }
        }
        List<LocalDateTime> delkeys = new ArrayList<>();
        int delvalue = 1;
        for (Map.Entry entry : mas.entrySet()) {
            if ((int) entry.getValue() == delvalue) {
                delkeys.add((LocalDateTime) entry.getKey());
            }
            delvalue = (int) entry.getValue();
        }
        for (LocalDateTime key : delkeys) {
            mas.remove(key);
        }
        //int duration = timePicking.getDuration();
        /*for (Map.Entry entry : mas.entrySet()) {

        }*/
        return mas;
    }

    public TreeMap meetingForTimeOn(TimePicking timePicking) {
        TreeMap<LocalDateTime, Integer> mas = new TreeMap<>();
        LocalDateTime begin = timePicking.getBegin().withSecond(0).withNano(0);
        LocalDateTime end = timePicking.getEnd().withSecond(0).withNano(0);
        LocalDateTime dateTime = begin;
        while (dateTime.isBefore(end)) {
            mas.put(dateTime, 0);
            dateTime = dateTime.plusMinutes(5);
        }
        List<UserTime> peopleUsertime = timePicking.getEvents();
        for (UserTime userTime : peopleUsertime) {
            for (Events eventInfoDTO1 : userTime.getEvents()) {
                if (eventInfoDTO1.getBegin().withSecond(0).withNano(0).isBefore(begin) && (eventInfoDTO1.getEnd().withSecond(0).withNano(0).isAfter(begin))) {
                    if (eventInfoDTO1.getEnd().withSecond(0).withNano(0).isAfter(end)) {
                        for (Map.Entry entry : mas.entrySet()) {
                            mas.replace((LocalDateTime) entry.getKey(), (Integer) entry.getValue() + 10);
                        }
                    } else {
                        LocalDateTime temp = begin.withSecond(0).withNano(0);
                        while (temp.isBefore(eventInfoDTO1.getEnd().withSecond(0).withNano(0))) {
                            mas.replace(temp, mas.get(temp) + 10);
                            temp = temp.plusMinutes(5);
                        }
                    }
                } else if (eventInfoDTO1.getBegin().withSecond(0).withNano(0).isBefore(end) && eventInfoDTO1.getEnd().withSecond(0).withNano(0).isAfter(end)) {
                    if (eventInfoDTO1.getBegin().withSecond(0).withNano(0).isBefore(begin)) {
                        for (Map.Entry entry : mas.entrySet()) {
                            mas.replace((LocalDateTime) entry.getKey(), (Integer) entry.getValue() + 10);
                        }
                    } else {
                        LocalDateTime temp = begin.withSecond(0).withNano(0);
                        while (temp.isBefore(eventInfoDTO1.getBegin().withSecond(0).withNano(0)))
                            temp = temp.plusMinutes(5);
                        while (temp.isBefore(end)) {
                            mas.replace(temp, mas.get(temp) + 10);
                            temp = temp.plusMinutes(5);
                        }
                    }
                } else {
                    LocalDateTime temp = begin.withSecond(0).withNano(0);
                    while (temp.isBefore(eventInfoDTO1.getBegin().withSecond(0).withNano(0)))
                        temp = temp.plusMinutes(5);
                    while (temp.isBefore(eventInfoDTO1.getEnd().withSecond(0).withNano(0))) {
                        mas.replace(temp, mas.get(temp) + 10);
                        temp = temp.plusMinutes(5);
                    }
                }
            }
        }
        List<LocalDateTime> delkeys = new ArrayList<>();
        int delvalue = 1;
        for (Map.Entry entry : mas.entrySet()) {
            if ((int) entry.getValue() == delvalue) {
                delkeys.add((LocalDateTime) entry.getKey());
            }
            delvalue = (int) entry.getValue();
        }
        for (LocalDateTime key : delkeys) {
            mas.remove(key);
        }
        return mas;
    }
}
